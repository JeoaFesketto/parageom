from os import getcwd, system
from warnings import warn
from inspect import signature

import numpy as np
import matplotlib.pyplot as plt
from geomdl import BSpline

from parageom.common import make_output_folder
from parageom.bin_functions.script_writers import make_ag_script

from parageom.reader.reader_cfg_3d import Cfg3D
from parageom.reader.reader_geomTurbo import GeomTurbo


# TODO add the blade object when it is available...
# TODO add a __repr__
# TODO make documentation


class Geometry:
    """
    The `Geometry` class stores all the different point clouds and curves associated with a
    given blade. The geometry can then be parametrized.

    Attributes
    ----------
    name : str
        Geometry name, extracted from file name if init is from file.
    scale_factor : float or int
        The scale factor of the geometry object. Mainly used for the parametrization process.
    n_blades : float or int
        Number of blades of the geometry.
    n_sections : float or int
        Number of sections contained in the point cloud. Read from the `pressure_section` array shape.
    n_points : float or int
        Number of points for each full (pressure and suction sides) section. Read from the `pressure_section` array
        shape.
    n_dim : float or int
        Number of coordinates for each point. Warning, 2D cases not tested or implemented yet. Read from the
        `pressure_section` array shape.
    leading_edge : numpy.ndarray
        Array of shape (n_points, n_dim) containing each point on axis 0 and each coordinate on axis 1 of the leading
        edge. Can either be read from the `edges` attribute of the `reader_object` or extracted from the
        `geometry_points` of that same object if it has no `edges` attribute.
    trailing_edge : numpy.ndarray
        Array of shape (n_points, n_dim) containing each point on axis 0 and each coordinate on axis 1 of the trailing
        edge. Can either be read from the `edges` attribute of the `reader_object` or extracted from the
        `geometry_points` of that same object if it has no `edges` attribute.
    pressure_sections : numpy.ndarray
        Array of shape (n_sections, n_points, n_dim), containing each section on axis 0, each point on axis 1 and each
        coordinate on axis 2. As the name suggests, this array contains only the points on the pressure side of the
        blade.
    suction_sections : numpy.ndarray
        Array of shape (n_sections, n_points, n_dim), containing each section on axis 0, each point on axis 1 and each
        coordinate on axis 2. As the name suggests, this array contains only the points on the suction side of the
        blade.
    section_coordinates : np.ndarray
        Array of shape (n_sections, n_points, n_dim), containing each section on axis 0, each point on axis 1 and each
        coordinate on axis 2. This array contains the points for the entire section from the leading edge, along the
        suction side, through the trailing edge, along the pressure side, back to the leading edge. If the
        `reader_object` has an attribute `section_coordinates`, it is used, otherwise, the `suction_section` and
        `pressure_section` arrays are stacked together to create it.
    

    Parameters
    ----------
    reader_object : ...
    """
    @classmethod
    def from_file(cls, file, **kwargs):

        if file.endswith('.geomTurbo'):
            reader_object = GeomTurbo
        elif file.endswith('.cfg'):
            reader_object = Cfg3D
        else:
            raise TypeError(f'.{file.split(".")[-1]} files are not yet supported.')

        sig = signature(reader_object.__init__)
        keywords = [p.name for p in sig.parameters.values()]
        kwargs = {key: value for key, value in kwargs.items() if key in keywords}
        
        return cls(reader_object(file, **kwargs))


    def __init__(self, reader_object):

        """The attributes have the following shapes:

            - self.sections_coordinates:
                of shape (N_sections, N_points, N_dim)

            - self.pressure_sections: of shape (N_sections, N_points_pressure, N_dim)

                with N_points the number of points per section and N_dim = 3
                N_points may be different on the pressure and suction sides if
                the geometry is read from an .geomTurbo file.


            # TODO check that it corresponds in fact to the number of sections

            - self.leading_edge: of shape (N_sections, N_dim)

            - self.no_sections: scalar

        """
        self.name = reader_object.filename
        self.pressure_sections = reader_object.geometry_points[0]
        self.suction_sections = reader_object.geometry_points[1]

        if reader_object.edges is not None:
            self.leading_edge = reader_object.edges[0]
            self.trailing_edge = reader_object.edges[1]
        else:
            self.leading_edge = reader_object.geometry_points[0][:, 0]
            self.trailing_edge = reader_object.geometry_points[0][:, -1]

        self.n_sections, _, self.n_dim = self.pressure_sections.shape
        self.n_points = self.pressure_sections.shape[1] * 2

        try:
            self.section_coordinates = reader_object.section_coordinates
        except:
            # TODO find way to vectorize this...
            self.section_coordinates = np.array(
                [
                    np.vstack((self.suction_sections[i], self.pressure_sections[i]))
                    for i in range(self.n_sections)
                ]
            )

        self.scale_factor = reader_object.scale_factor

        self.n_blades = reader_object.N_blades

    def parablade_section_export(self, section_idx, file=None, dim="2D", is_new=True):

        """
        Writes the point cloud of a section to a txt file
        """

        section = self.section_coordinates[section_idx] * self.scale_factor

        if file is None:
            file = (
                "./confidential/blade.txt"
                if dim == "2D"
                else "./confidential/3Dblade.txt"
            )

        mode = "w" if is_new else "a"
        with open(file, mode) as f:
            if dim == "2D":
                f.writelines(
                    [
                        f"{i}\t{section.T[2, i]}\t{section.T[0, i]}\n"
                        for i in range(len(section.T[0]))
                    ]
                )
            elif dim == "3D":
                f.writelines(
                    [
                        f"{i}\t{section.T[0, i]}\t{section.T[1, i]}\t{section.T[2, i]}\n"
                        for i in range(len(section.T[0]))
                    ]
                )
        print(f"Done exporting to {file}")

    def parablade_blade_export(self, file=None, iterator=None):

        """
        Writes the point cloud of the 3D blade to a txt file
        """
        iterator = (
            np.array(np.linspace(0, 180, 10), dtype="int")
            if iterator is None
            else iterator
        )
        self.parablade_section_export(iterator[0], file=file, dim="3D")
        for i in iterator[1:]:
            self.parablade_section_export(i, file=file, dim="3D", is_new=False)

    def plot_section(self, section_idx):

        section = self.section_coordinates[section_idx]

        ax = plt.axes(projection="3d")
        ax.plot3D(section.T[0], section.T[1], section.T[2], "gray")

        ax.set_xlim(-100, 30)
        ax.set_zlim(0, 180)
        ax.set_ylim(-250, 200)
        ax.set_box_aspect(
            [ub - lb for lb, ub in (getattr(ax, f"get_{a}lim")() for a in "xyz")]
        )

        plt.show()

    # TODO change the way the arguments are passed in for the fillets.
    def output_geomTurbo(
        self,
        filename="output.geomTurbo",
        LE_fillet=False,
        TE_fillet=False,
        xyz="xyz",
    ):
        """This function outputs a geomTurbo file of the blade ready to be read and used
        in autogrid.


        N_sections: scalar, number of sections.
        N_points: scalar, best if even, odd number of points not yet tested"""

        shape = self.section_coordinates.shape
        if shape[1] % 2 == 0:
            tmp = self.section_coordinates
            tmp = tmp.reshape((shape[0], 2, shape[1] // 2, 3))
            tmp[:, 0, -1] = tmp[:, 1, 0]  # matching the trailing edge
        else:
            warn(
                "Odd number of points not yet tested. If problems occur, it might be due to this."
            )
            middle = int(shape[1] / 2 - 0.5)
            tmp = np.insert(
                self.section_coordinates,
                middle,
                self.section_coordinates[:, middle, :],
                axis=1,
            )
            tmp = tmp.reshape((shape[0], 2, tmp.shape[1] // 2, 3))
        tmp[:, 1] = np.flip(tmp[:, 1], axis=1)
        tmp[..., [0, 1, 2]] = tmp[..., [1, 2, 0]]

        self.split_coordinates = tmp

        if LE_fillet:
            self.split_coordinates = self._LE_fillet(
                self.split_coordinates,
                cutoff_percentage=self.blade.IN["LE_FILLET_cutoff_percentage"],
                min_angle=self.blade.IN["LE_FILLET_min_angle"],
            )
        if TE_fillet:
            self.split_coordinates = self._TE_fillet(
                self.split_coordinates,
                cutoff_percentage=self.blade.IN["TE_FILLET_cutoff_percentage"],
                min_angle=self.blade.IN["TE_FILLET_min_angle"],
            )

        self.write_geomTurbo(filename, xyz=xyz)

    def _LE_fillet(self, point_cloud, N_le=30, cutoff_percentage=0.08, min_angle=15):
        """
        Sub-function that rounds the leading edge based on a couple parameters.
        The defaults seem to work quite well.

        N_le is the number of newly generated points at the leading edge.
        min_width is the width of the blade under which the blade will be cut.

        With p1 the point along the section surface at which the blade is to be cut, the angle alpha
        corresponds to the angle between the tangent to the surface at p1 and the line between
        p1 and the leading edge point.

        min_angle is the threshold before which the leading edge is moved in to shorten the blade
        and increase the angle.
        min_angle should be input in degrees.

        """
        # NOTE there is a slight problem with the number of points.
        # the array is expanded by N_le-1 points actually...
        min_angle = np.deg2rad(min_angle)
        tmp = point_cloud
        final_array = np.zeros((tmp.shape[0], 2, tmp.shape[2] + N_le - 1, 3))

        # get approx length of camberline of first section
        i, camberline_length = 1, 0
        while tmp.shape[2] > i:
            camberline_length += _dist(
                _centre(*tmp[0, :, i]), _centre(*tmp[0, :, (i - 1)])
            )
            i += 1

        for k, section in enumerate(tmp):
            le = section[0, 0]

            i = 1
            while (
                _dist(le, _centre(*section[:, i])) / camberline_length
                < cutoff_percentage
            ):
                i += 1
                elem = section[:, i]

            suction_curve = BSpline.Curve()
            pressure_curve = BSpline.Curve()
            suction_curve.degree = 2
            pressure_curve.degree = 2

            le = section[0, 0]
            tangent_vectors = section[:, i] - section[:, i + 1]
            normal_vector = section[0, i] - section[1, i]
            ce = section[1, i] + 1 / 2 * normal_vector

            if np.min(_angle(tangent_vectors, le - section[:, i])) < min_angle:
                j = 1
                while (
                    np.min(
                        _angle(tangent_vectors, _centre(*section[:, j]) - section[:, i])
                    )
                    < min_angle
                ):
                    j += 1
                le = _centre(*section[:, j])

            suction_ctrl = _get_intersect(
                le, section[0, i], tangent_vectors[0], le - ce
            )
            pressure_ctrl = _get_intersect(
                le, section[1, i], tangent_vectors[1], le - ce
            )

            suction_curve.ctrlpts = [list(elem[0]), suction_ctrl, list(le)]
            pressure_curve.ctrlpts = [list(elem[1]), pressure_ctrl, list(le)]

            suction_curve.knotvector = [0, 0, 0, 2, 2, 2]
            pressure_curve.knotvector = [0, 0, 0, 2, 2, 2]

            suction_curve.delta = 1 / (N_le + i)
            pressure_curve.delta = 1 / (N_le + i)

            new_head = np.array(
                [
                    np.flip(suction_curve.evalpts, axis=0),
                    np.flip(pressure_curve.evalpts, axis=0),
                ]
            )

            final_array[k, :] = np.concatenate((new_head, tmp[k, :, i + 1 :]), axis=1)

        self.n_points += 2 * (N_le - 1)
        return final_array

    def _TE_fillet(self, point_cloud, N_te=30, cutoff_percentage=0.05, min_angle=6):
        """
        Sub-function that rounds the trailing edge based on a couple parameters.
        The defaults seem to work quite well.

        N_te is the number of newly generated points at the trailing edge.
        min_width is the width of the blade under which the blade will be cut.

        With p1 the point along the section surface at which the blade is to be cut, the angle alpha
        corresponds to the angle between the tangent to the surface at p1 and the line between
        p1 and the trailing edge point.

        min_angle is the threshold before which the trailing edge is moved in to shorten the blade
        and reduce the angle.
        min_angle should be input in degrees.

        """

        min_angle = np.deg2rad(min_angle)
        tmp = point_cloud
        final_array = np.zeros((tmp.shape[0], 2, tmp.shape[2] + N_te, 3))

        # get approx length of camberline of first section
        i, camberline_length = 1, 0
        while tmp.shape[2] > i:
            camberline_length += _dist(
                _centre(*tmp[0, :, i]), _centre(*tmp[0, :, (i - 1)])
            )
            i += 1

        for k, section in enumerate(tmp):
            te = section[0, -1]

            i = 1
            while (
                _dist(te, _centre(*section[:, -i])) / camberline_length
                < cutoff_percentage
            ):
                i += 1
                elem = section[:, -i]

            suction_curve = BSpline.Curve()
            pressure_curve = BSpline.Curve()
            suction_curve.degree = 2
            pressure_curve.degree = 2

            tangent_vectors = section[:, -i] - section[:, -i - 1]
            normal_vector = section[0, -i] - section[1, -i]
            ce = section[1, -i] + 1 / 2 * normal_vector

            if np.min(_angle(tangent_vectors, te - section[:, -i])) < min_angle:
                j = 1
                while (
                    np.min(
                        _angle(
                            tangent_vectors, _centre(*section[:, -j]) - section[:, -i]
                        )
                    )
                    < min_angle
                ):
                    j += 1
                te = _centre(*section[:, -j])

            suction_ctrl = _get_intersect(
                te, section[0, -i], tangent_vectors[0], te - ce
            )
            pressure_ctrl = _get_intersect(
                te, section[1, -i], tangent_vectors[1], te - ce
            )

            suction_curve.ctrlpts = [list(elem[0]), suction_ctrl, list(te)]
            pressure_curve.ctrlpts = [list(elem[1]), pressure_ctrl, list(te)]

            suction_curve.knotvector = [0, 0, 0, 2, 2, 2]
            pressure_curve.knotvector = [0, 0, 0, 2, 2, 2]

            suction_curve.delta = 1 / (N_te + i)
            pressure_curve.delta = 1 / (N_te + i)

            new_tail = np.array([suction_curve.evalpts, pressure_curve.evalpts])

            final_array[k, :] = np.concatenate((tmp[k, :, :-i], new_tail), axis=1)

        self.n_points += 2 * N_te
        return final_array

    def write_geomTurbo(self, filename="output.geomTurbo", xyz="xyz"):

        xyz = xyz.replace("x", "0").replace("y", "1").replace("z", "2")
        xyz = [int(xyz[0]), int(xyz[1]), int(xyz[2])]

        lines = [
            "GEOMETRY TURBO VERSION 5",
            f"number_of_blades {int(self.n_blades)}",
            "blade_expansion_factor_hub  0.01",
            "blade_expansion_factor_shroud 0.01",
            "blade_tangential_definition	  0",
        ]

        for k, side in enumerate(["suction", "pressure"]):
            lines.append(side)
            lines.append("SECTIONAL")
            lines.append(str(self.split_coordinates.shape[0]))

            for i, section in enumerate(self.split_coordinates[:, k]):
                lines.append(f"# section {i+1}")
                lines.append("XYZ")
                lines.append(str(self.split_coordinates.shape[2]))

                for point in np.asarray(
                    np.asarray(section, dtype="float"), dtype="str"
                ):
                    lines.append(" ".join(point[xyz]))

        with open(filename, "w") as f:
            f.write("\n".join(lines))
            f.write("\n")  # don't remove it

    def make_mesh(self, trb_file, output_dir='to_run', row_number=1, auto_run=False):

        DIR = getcwd()
        make_output_folder(output_dir)

        with open(f"{output_dir}/RUN.ME", "w") as f: f.write("module load fine")

        self.output_geomTurbo(f"{output_dir}/{self.name}.geomTurbo")

        make_output_folder(f"{output_dir}/mesh")

        options = {
            "_CASE_NAME_": self.name,
            "_TEMPLATE_": trb_file,
            "_GEOMTURBO_": f"{DIR}/{output_dir}/{self.name}.geomTurbo",
            "_OUTPUT_DIR_": f"{DIR}/{output_dir}/mesh",
            "_ROW_NUMBER_": str(row_number),
        }
        
        script_output_file = (
            f"{output_dir}/ag_script_{self.name}.py"
        )

        make_ag_script(options, script_output_file=script_output_file)

        with open(f"{output_dir}/RUN.ME", "a") as f:
            f.write(f"igg -autogrid5 -real-batch -script {script_output_file}\n")
        
        if auto_run:
            system(f". {output_dir}/RUN.ME")
        else:
            print(
                "\n\nPreparation of scripts done. Run `RUN.ME` from here when you are ready to mesh the geometries."
                f"\nUse command `. {output_dir}/RUN.ME`"
            )

# Functions used in _LE_fillet and _TE_fillet.

def _dist(p1, p2):
    return np.linalg.norm(p2 - p1)


def _angle(v1, v2):
    """
    Gets the angle between vector elements of two similarly sized arrays.
    Returns:
        numpy.ndarray of shape N_vectors
    """
    return np.arccos(
        np.diag(np.tensordot(v1, v2, axes=[1, 1]))
        / (np.linalg.norm(v1, axis=1) * np.linalg.norm(v2, axis=1))
    )


def _centre(p1, p2):
    return p1 + (p2 - p1) / 2


def _get_intersect(p1, p2, v1, n):
    n_hat = n / np.linalg.norm(n)
    k = np.matmul(p1.T, n_hat)
    t = (k - np.dot(p2, n_hat)) / np.dot(v1, n_hat)
    return list(p2 + t * v1)
