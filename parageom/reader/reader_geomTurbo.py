import numpy as np

# TODO change the methods to class methods to standardise how the reader objects work
class GeomTurbo:
    def __init__(self, file, scale_factor=1, init="sectioned", xyz="xyz"):

        self.file_path = file
        self.filename = file.split("/")[-1][:-10]
        self.scale_factor = scale_factor
        self.xyz = xyz
        self.geometry_points = None
        self.parameters = None
        self.surfaces = None
        self.curves = None
        self.edges = None
        self.N_blades = None

        if init == "unsectioned":
            self.read_unsectioned_geomTurbo(self.file_path)
        elif init == "sectioned":
            self.read_sectioned_geomTurbo(self.file_path)
        else:
            raise NotImplementedError()

    def read_unsectioned_geomTurbo(self, file_path=None):

        """
        This functions reads a sectioned geomTurbo file and returns a numpy array.

        self.surfaces is a list of 4 np.arrays corresponding to the pressure and suction
        sides of the rotor and the stator.
        For the np.arrays:  axis 0:     the various horizontal sections
                            axis 1:     the cloud of points of the section
                                        (only for one side)
                            axis 2:     each point coordinate

        """

        file = self.file_path if file_path == None else file_path

        with open(file, "r") as f:
            data = [
                " ".join(line.rstrip("\n").replace("\t", " ").split())
                for line in f.readlines()
            ]

        geom = [data[i].split(" ") for i in range(896)]
        igg = data[897:]

        self.edges = np.array([geom[99:280], geom[283:464]], dtype="float32")
        self.stator_edges = np.array([geom[535:704], geom[707:876]], dtype="float32")
        # TODO trouver un meilleur moyen de chopper les bons indexes

        surfaces_shapes = []
        for i, line in enumerate(igg):
            igg[i] = line.split(" ")
            if igg[i][0].startswith("SISLS"):
                surfaces_shapes.append(list(map(int, igg[i - 1][2:4])))

        curves, i, tmp_curve, adding = [], 0, [], False

        while i < len(igg):
            try:
                igg[i] = list(map(float, igg[i]))
                if len(igg[i]) == 3:
                    if not adding:
                        adding = True
                    tmp_curve.append(igg[i])
                else:
                    if adding:
                        curves.append(np.array(tmp_curve))
                        tmp_curve = []
                        adding = False
            except:
                pass
            i += 1

        curves = list(filter(lambda x: len(x) != 1, curves))
        surfaces, curves = curves[-4:], curves[:-4]

        for i, surface in enumerate(surfaces):
            surfaces[i] = np.array(np.vsplit(surface, surfaces_shapes[i][1]))
            surfaces[i] = surfaces[i].transpose((1, 0, 2))

        self.curves = curves
        self.surfaces = surfaces

        self.geometry_points = [surfaces[0], surfaces[1]]

    def read_sectioned_geomTurbo(self, file_path=None):

        """

        This functions reads a sectioned geomTurbo file and returns a numpy array.

        The output self.rotor_points is structured as follows:

        axis 0:     pressure and suction sides
        axis 1:     the various horizontal sections
        axis 2:     the cloud of points of the section
        axis 3:     the coordinates of the points

        """
        # TODO make it auto to get the number of sections and the number of points
        file = self.file_path if file_path == None else file_path

        with open(file, "r") as f:
            raw_content = f.readlines()

        N_sections = int("".join(raw_content).split("\n")[7])
        N_points = int("".join(raw_content).split("\n")[10])
        self.N_blades = int("".join(raw_content).split("\n")[1].split(" ")[-1])

        content = "".join(raw_content).replace(
            f"pressure\nSECTIONAL\n{N_sections}\n", ""
        )

        content = content.split(f"suction\nSECTIONAL\n{N_sections}\n")[1]
        content = content.split(f"XYZ\n{N_points}\n")
        for i, contour in enumerate(content):
            content[i] = contour.split("\n")
        content = content[1:]

        for i in range(len(content)):
            if i != len(content) - 1:
                content[i] = content[i][:-2]
            else:
                content[i] = content[i][:-1]
            for j in range(len(content[i])):
                content[i][j] = content[i][j].split(" ")

        self.geometry_points = np.array(content, dtype="float").reshape(
            (2, N_sections, N_points, 3)
        )

        if self.xyz != "xyz":
            xyz = self.xyz.replace("x", "0").replace("y", "1").replace("z", "2")
            self.geometry_points = self.geometry_points[
                ..., [int(xyz[0]), int(xyz[1]), int(xyz[2])]
            ]

