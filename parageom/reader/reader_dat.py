import os
import numpy as np

# TODO change name to be consistent with other reader objects

class DatReader:
    def __init__(self, file, **kwargs):

        self.file_path = file
        self.filename = file.split('/')[-1].split('.')[0]
        self.geometry_points = [get_kwarg('pressure_side', kwargs), get_kwarg('suction_side', kwargs)]

        self.edges = [get_kwarg('leading_edge', kwargs), get_kwarg('trailing_edge', kwargs)]
        if self.edges[0] is None:
            self.edges = None

        self.scale_factor = 1 if get_kwarg('scale_factor', kwargs) is not None else get_kwarg('scale_factor', kwargs)
        self.N_blades = get_kwarg('number_of_blades', kwargs)

    @classmethod
    def from_suction_and_pressure_sides(cls, folder, le_te = True):
        pressure_side_files = []
        suction_side_files = []
        for file in os.listdir(folder):
            if 'pressure' in file.lower():
                pressure_side_files.append(file)
            elif 'suction' in file.lower():
                suction_side_files.append(file)
            
            else:
                if le_te:
                    if 'leading' in file.lower():
                        leading_edge = file
                    elif 'trailing' in file.lower():
                        trailing_edge = file

        pressure_side_files = sorted(pressure_side_files)
        suction_side_files = sorted(suction_side_files)

        
        pressure_side = np.stack(tuple(np.flip(np.loadtxt(f'{folder}/{file}'), axis=0) for file in pressure_side_files))
        suction_side = np.stack(tuple(np.loadtxt(f'{folder}/{file}') for file in suction_side_files))
        if le_te:
            return cls(
                f'{folder}/{pressure_side_files[0]}',
                pressure_side = pressure_side,
                suction_side = suction_side,
                leading_edge = leading_edge,
                trailing_edge = trailing_edge,
            )
        else:
            return cls(
                f'{folder}/{pressure_side_files[0]}',
                pressure_side = pressure_side,
                suction_side = suction_side,
            )

        

def get_kwarg(key, kwargs, warn = False):
    try:
        return kwargs[key]
    except KeyError:
        if warn:
            warn(f'{key} not in keyword arguments')
        return None
    except Exception as e:
        raise e
        