import numpy as np
import parablade as pb

# TODO adapt this to work with the Geometry object
class Cfg2D:
    def __init__(self, file, no_points=100):

        """

        Creates a tmp rotor from blade parameters. The file path specified should be to
        a camber-thickness cfg file for parablade.

        Parameters
        ----------
        file :          path to the camber thickness .cfg file with the desired blade parameters
        no_points :     scalar of type int with the number of points wanted for each section
                        to later become the dimension N_points in the Rotor object

        """

        blade = pb.Blade2DCamberThickness(file)
        blade.get_section_coordinates(np.linspace(0.00, 1.00, no_points))

        self.rotor_points = [
            np.asarray(blade.pressure_coordinates, dtype="float32"),
            np.asarray(blade.suction_coordinates, dtype="float32"),
        ]

        z_coords = np.zeros((blade.section_coordinates.T.shape[0], 1))
        self.section_coordinates = np.array(
            [np.hstack((blade.section_coordinates.T, z_coords))]
        )

        self.rotor_edges = None