import numpy as np
import parablade as pb


class Cfg3D:
    def __init__(self, file, N_sections=181, N_points=362, UV=None):

        """
        attributes:

            section_coordinates: ndarray of shape (N_sections, N_points, N_dim)
            with on axis 0: all the sections
                    axis 1: all the points
                    axis 2: the coordinates

            split_coordinates: ndarray of shape(2, N_sections, N_points, N_dim)
            with the suction and pressure side respectively on axis 0.
        """
        self.filename = file.split("/")[-1][:-4]

        self.blade = pb.Blade3D(file, UV=UV)
        self.blade.make_blade()

        tmp1 = np.geomspace(1e-2, 0.5, N_points // 4) - 1e-2
        tmp2 = np.flip(np.ones(tmp1.shape) - tmp1)
        u = np.hstack((tmp1, tmp2)) * 0.5
        u = np.hstack((u, np.flip(np.ones(u.shape) - u)))
        V = np.linspace(0, 1, N_sections)

        self.N_blades = self.blade.IN["N_BLADES"]

        self.N_sections = N_sections
        self.N_points = N_points
        self.section_coordinates = np.real(np.array(
            list(map(lambda v: self.blade.get_section_coordinates(u, v).T, V))
        ))
        # ^ this does this:
        # self.section_coordinates = []
        # for i in v:
        #     self.section_coordinates.append(blade.get_section_coordinates(u, i).T)
        # self.section_coordinates = np.real(np.array(self.section_coordinates))

        self.split_coordinates = None

        self.edges = [
            self.section_coordinates[:, 0], 
            self.section_coordinates[:, self.section_coordinates.shape[1]//2]
        ]
        self.geometry_points = [
            self.section_coordinates[:, :self.section_coordinates.shape[1]//2],
            np.flip(self.section_coordinates[:, self.section_coordinates.shape[1]//2:], axis=1),
        ]

        self.scale_factor = self.blade.IN['SCALE_FACTOR']
        self.N_blades = self.blade.IN['N_BLADES'][0]