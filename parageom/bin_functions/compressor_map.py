from parageom.bin_functions.fine_project import FineProject
import numpy as np
import pickle


class CompressorMap:

    def __init__(self, project: FineProject):

        self.project = FineProject(project)
        self.results = self.project.run_computations_results
        self.rpm_set = np.array(sorted(set(self.results[:, 0])))
        self.sorted_computations = {}
        for rpm in self.rpm_set:
            self.sorted_computations[str(int(rpm))] = [
                computation for _, computation in self.project.computations_dict.items()
                if computation.bc[0] == rpm
            ]
        
    def get_existing_iso_lines(self):
        self.iso_lines = {}
        for key, item in self.sorted_computations.items():
            self.iso_lines[key] = IsoLine(item)
    
    def make_iso_line(self, rpm):
        initialisation_rpm = min(self.rpm_set, key=lambda x:abs(x-rpm))
        initialisation_iso_line = self.iso_lines[str(initialisation_rpm)]
        new_pressures = np.linspace(
            initialisation_iso_line.predicted_limit_left, 
            initialisation_iso_line.predicted_limit_right,
            6
        )
        
    
    

class IsoLine:
    def __init__(self, computations):
        self.computations = sorted(computations, key=lambda x: x.bc[1])
        self.rpm = self.computations[0].bc[0]

        self.centre = sorted(
            [computation for computation in self.computations if computation._has_run],
            key = lambda x: x.results.efficiency
        )[-1]
        if not self.get_precision_left():
            self.predicted_limit_left = None
            self.precision_left = None
        if not self.get_precision_right():
            self.predicted_limit_right = None
            self.precision_right = None
    
    def get_precision_left(self):
        for i in range(len(self.computations)-1):
            j = i+1
            if (not self.computations[i]._has_run) and self.computations[j]._has_run:
                self.predicted_limit_left = (self.computations[j].bc[1] + self.computations[i].bc[1])/2
                self.precision_left = 1/(self.computations[j].bc[1] - self.computations[i].bc[1])*100
                return True
        return False
    
    def get_precision_right(self):
        for i in range(len(self.computations)-1):
            j = i+1
            if (not self.computations[-i]._has_run) and self.computations[-j]._has_run:
                self.predicted_limit_right = (self.computations[-j].bc[1] + self.computations[-i].bc[1])/2
                self.precision_right = 1/(self.computations[-j].bc[1] - self.computations[-i].bc[1])*100
                return True
        return False
     