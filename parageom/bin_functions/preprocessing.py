import time

import matplotlib.pyplot as plt
import numpy as np

from parageom.reader.reader_geomTurbo import GeomTurbo
from parageom.reader.reader_cfg_3d import Cfg3D
from parageom.geometry import Geometry
from parageom.common import recursive_get_files

def show_section(
    *files,
    span_percentage=0,
):
    def _get_orientation(reference_orientation_geometry, geometry):
        for i, combination in enumerate(
            [
                [0, 1, 2],
                [0, 2, 1],
                [1, 0, 2],
                [1, 2, 0],
                [2, 0, 1],
                [2, 1, 0],
            ]
        ):
            current = np.linalg.norm(
                geometry.leading_edge[0, combination]
                - reference_orientation_geometry.leading_edge[0],
            )

            if i == 0:
                best = current
                to_return = combination

            elif current < best:
                best = current
                to_return = combination

        return to_return

    # TODO implement 2D plotting
    t = time.time()

    ax = plt.axes(projection="3d")
    colors = ["red", "blue", "yellow", "orange", "green", "lime", "pink", "purple"]

    for i, file in enumerate(files):

        if file.endswith(".cfg"):
            geometry = Geometry(Cfg3D(file))

        elif file.endswith(".geomTurbo"):
            geometry = Geometry(GeomTurbo(file))

        else:
            raise TypeError(f'file type not supported: .{file.split(".")[-1]}')

        if i == 0 or file.endswith(".cfg"):
            reference_orientation_geometry = geometry
            orientation = [0, 1, 2]

        else:
            orientation = _get_orientation(reference_orientation_geometry, geometry)

        le_points = geometry.leading_edge
        section = _le_section_getter(le_points, span_percentage)

        points = np.vstack(
            (
                geometry.suction_sections[section],
                np.flip(geometry.pressure_sections[section], axis=0),
            )
        )[..., orientation]

        ax.plot3D(points.T[0], points.T[2], points.T[1], colors[i])
        print(f"\t\tplotted {file} in {colors[i]}")

        if i == 0:
            le = points[0]
            te = geometry.trailing_edge[section]

    ax.set_xlim(le[0] - le[1] / 2, te[0] + le[1] / 2)
    ax.set_zlim(le[1] - 0.3, te[1] + 0.3)
    ax.set_ylim(le[2] - 0.3, te[2] + 0.3)
    ax.set_box_aspect(
        [ub - lb for lb, ub in (getattr(ax, f"get_{a}lim")() for a in "xyz")]
    )

    print(
        "This was generated in %(my_time).5f seconds\n" % {"my_time": time.time() - t}
    )

    plt.show()


# TODO move this to the geometry.py file either as method or function
def _le_section_getter(le_points, span_percentage):
    target = (le_points[-1] - le_points[0]) * (span_percentage * 0.01) + le_points[0]
    prev = np.linalg.norm(le_points[0] - target)
    i = 1
    while prev > np.linalg.norm(le_points[i] - target):
        prev = np.linalg.norm(le_points[i] - target)
        i += 1
        if i == le_points.shape[0]:
            break
    return i - 1


def read_residuals(filename, all_data=False):
    """
    Returns
    -------
    out : numpy.ndarray
    outputs numpy array of shape (n_iter, 7), with the columns in this order:
    iteration, axial thrust, torque, eta, pressure ratio, mflow in, mflow out
    """
    # reading .res file
    with open(filename, "r") as f:
        data = f.readlines()

    # removing header
    data = data[10:]

    # retaining only the lines with global metrics
    global_data = [line.split() for line in data if line.split()[0].isdigit()]

    # turning the data into array and retaining useful metrics only
    full_array = np.array(global_data, dtype=float)

    return full_array if all_data else full_array[:, [0, 3, 5, 6, 7, 8, 9]]


def read_multiple_residuals(input_directory, all_data=False):

    outputs = []
    for file in recursive_get_files(input_directory, ".res"):
        outputs.append((file, read_residuals(file, all_data)))

    return outputs