import os
from warnings import warn
from dataclasses import dataclass

import numpy as np
from shutil import copy
import matplotlib.pyplot as plt

from parageom.common import recursive_get_files, make_output_folder, get_size, rpm_to_rad


class FineProject:
    def __init__(self, iec_file):

        # check that the path inputted exists and that the file is indeed an iec file.
        if not iec_file.endswith(".iec"):
            raise TypeError(
                f"{iec_file} does not have the required extension (`.iec`). If the extension was not included, please include it."
            )
        if not os.path.isfile(iec_file):
            raise TypeError(f"{iec_file} is either not a valid path or not a file.")

        self.iec_file = iec_file
        self.name = f'{iec_file.split("/")[-1].split(".")[-2]}'

        if os.path.isabs(self.iec_file):
            self.parent_folder = f'{"/".join(iec_file.split("/")[:-1])}'
        else:
            self.parent_folder = (
                f'{os.getcwd()}/{"/".join(iec_file.split("/")[:-1])}'
                if "/" in iec_file
                else os.getcwd()
            )

        with open(self.iec_file, 'r') as f: self.mesh_file = f.readlines()[4].split()[-1]

        self.computations_dict = {}
        for run_file in recursive_get_files(self.parent_folder, ".run"):
            tmp_computation = FineComputation.from_run_file(run_file)
            self.computations_dict[tmp_computation.name] = tmp_computation
        
        self.run_computations_results = None
        self.get_computations_results()

        self.project_size = get_size(self.parent_folder)
    
    def __repr__(self) -> str:
        to_print =  (
            f"\nname:\t\t\t{self.name}\n"
            f"parent folder:\t\t{self.parent_folder}\n"
            f"full path:\t\t{self.parent_folder}/{self.name}.iec\n"
            f"mesh file:\t\t{self.mesh_file}\n"
            f"size on disk:\t\t{self.project_size:.2f} GB\n"
            f"number of computations: {len(self.computations_dict)}\n"
        )
        return to_print
    
    def get_computations_results(self):

        existing_computations = []
        for _, computation in self.computations_dict.items():
            if computation._has_run:
                if computation._is_converged:
                    existing_computations.append(
                        np.hstack((
                            np.array(computation.bc),
                            computation.results.efficiency,
                            computation.results.pressure_ratio,
                            computation.results.mass_flow_in,
                        ))
                    )
                else:
                    existing_computations.append(
                        np.hstack((np.array(computation.bc), np.zeros(3)))
                    )
        self.run_computations_results = np.array(existing_computations) 

    def add_computation(self, boundary_conditions, initial_solution):

        new_computation = FineComputation.from_BCs(
            boundary_conditions, self, initial_solution
        )
        self.computations_dict[new_computation.name] = new_computation

        return new_computation

    def add_multiple_computations(self, computations_list):

        computations_object_list = []
        for i, computation in enumerate(computations_list):

            if len(computation) == 2:
                initial_solution = computations_list[i - 1][:2]

            elif len(computation) == 3:
                initial_solution = computation[2]

            elif len(computation) == 4:
                initial_solution = computation[2:]

            computation_object = self.add_computation(computation[:2], initial_solution)
            computations_object_list.append(computation_object)

        return computations_list

    def make_computation(self, name_or_bc):

        if isinstance(name_or_bc, tuple):
            name = _get_name_from_bc(name_or_bc, self)
        else:
            name = name_or_bc

        new = self.computations_dict[name]

        for _, computation in self.computations_dict.items():
            if computation._is_created and computation._is_parallelised:
                old = computation
                break

        # check if a template computation could be found.
        if not ("old" in locals() or "old" in globals()):
            raise TypeError(
                (
                    "No adequate computation could be found to be used as template for the new one. "
                    "Ensure there is at least one parallelised computation in the project."
                )
            )

        make_output_folder(new.parent_folder, overwrite=True)

        copy(old.get_file("p4pg"), new.get_file("p4pg"))

        copy(old.get_file("steering"), new.get_file("steering"))

        with open(new.get_file("steering"), "r") as f:
            data = f.read()
        data = data.replace(old.name, new.name)
        with open(new.get_file("steering"), "w") as f:
            f.write(data)

        copy(old.get_file("batch"), new.get_file("batch"))

        with open(new.get_file("batch"), "r") as f:
            data = f.read()
        data = data.replace(old.name, new.name)
        with open(new.get_file("batch"), "w") as f:
            f.write(data)

        copy(old.run_file, new.run_file)

        with open(new.run_file, "r") as f:
            data = f.read()
        # want name of computation without project name prefix
        # slicing is to remove leading underscore
        data = data.replace(
            old.name.replace(self.name, "")[1:], new.name.replace(self.name, "")[1:]
        )
        data = data.split("\n")

        for i, line in enumerate(data):

            if "INITIAL_SOLUTION" in line:
                line = line.replace(
                    old.initial_solution_file.split("/")[-1][:-4],
                    new.initial_solution_file.split("/")[-1][:-4],
                )

            if "REAL_ENTRY" in line:
                if str(old.bc[1]) in line:
                    line = line.replace(str(old.bc[1]), str(new.bc[1]))

                if "Block_Rotation_Speed" in line and line.split()[2] != "0":
                    line = line.replace(line.split()[2], str(rpm_to_rad(new.bc[0])))

                if "Rotational Speed 1" in line and line.split()[4] != "0":
                    line = line.replace(line.split()[4], str(rpm_to_rad(new.bc[0])))

            data[i] = line
        with open(new.run_file, "w") as f:
            f.write("\n".join(data))

        return new

    def make_multiple_computations(self):

        computations_list = []
        for _, computation in self.computations_dict.items():
            if not computation._is_created:
                new_computation = self.make_computation(computation.name)
                computations_list.append(new_computation)

        return computations_list

    def show_computations(self, computations_list=None, rpm=None, pressure=None, converged_only=None):

        if computations_list is None:
            for _, computation in self.computations_dict.items():
                filtered = False
                if rpm is not None and rpm != computation.bc[0]:
                    filtered = True
                    continue
                if pressure is not None and pressure != computation.bc[1]:
                    filtered = True
                    continue
                if converged_only is not None and not computation._is_converged == converged_only:
                    filtered = True
                    continue
                if not filtered:
                    print(computation)
        else:
            for computation in computations_list:
                print(computation)

    def cleanup(self, auto=False):
        flagged_for_deletion = []
        for _, computation in self.computations_dict.items():
            if computation._has_run and not computation._is_converged:
                flagged_for_deletion.append(computation)
        
        if auto:
            for computation in flagged_for_deletion:
                os.system(f"rm -rf {computation.parent_folder}")
                os.system(f"rm -rf job_{computation.name}")
                os.system(f"rm -rf out_{computation.name}")
        else:
            print('Computations that should be deleted.')
            for computation in flagged_for_deletion:
                print(computation)
    
    def change_mesh(self, new_mesh_file):
        with open(self.iec_file, 'r') as f: data = f.read()
        with open(self.iec_file, 'w') as f: f.write(data.replace(self.mesh_file, new_mesh_file))

        for _, computation in self.computations_dict.items():
            with open(computation.run_file, 'r') as f: data = f.read()
            with open(computation.run_file, 'w') as f: f.write(data.replace(self.mesh_file, new_mesh_file))
        
        self.mesh_file = new_mesh_file

    def copy_and_change_mesh(self, output_folder, project_name, mesh_file):
        new_parent_folder = f'{output_folder}/{project_name}'
        make_output_folder(new_parent_folder)

        new_project_iec_file = f"{new_parent_folder}/{project_name}.iec"
        copy(self.iec_file, new_project_iec_file)

        for _, computation in self.computations_dict.items():
            if computation._is_parallelised:
                continue
        computation_parent_folder = f"{new_parent_folder}/{project_name}_{computation.bc[0]}rpm_{int(computation.bc[1]/1000)}kp"
        make_output_folder(computation_parent_folder)

        copy(computation.run_file, f'{computation_parent_folder}/{computation.name}.run')
        copy(computation.get_file('batch'), f'{computation_parent_folder}/{computation.name}.batch')
        copy(computation.get_file('steering'), f'{computation_parent_folder}/{computation.name}.steering')
        copy(computation.get_file('p4pg'), f'{computation_parent_folder}/{computation.name}.p4pg')

        new_project = FineProject(new_project_iec_file)
        new_project.change_mesh(mesh_file)
        return new_project

    def save_results_to_csv(self, output_file='results.csv'):
        np.savetxt(
            output_file, 
            self.run_computations_results, 
            delimiter=', ', 
            header='bc1 (rpm), bc2 (kPa), eta, pressure ratio, mass flowrate (kg/s)',
        )

    def plot_compressor_map(self):

        data = self.run_computations_results
        plt.scatter(*data[:, [4, 3]].T, c=data[:, 2], marker="x", cmap='viridis')
        plt.colorbar()

        def _bc_to_output(results_array, bc_point):
            """Get the results corresponding to specified boundary conditions. Basically doing `list_bc.index(bc_point)` and `results_list[bc_point_index]`"""
            for point in results_array:
                if (point[[0, 1]] == bc_point).all():
                    return point[[4, 3]]

        BCs = data[:, [0, 1]]
        sorted_BCs = BCs[BCs[:, 1].argsort()]

        rpm_set = set(BCs[:, 0])

        # plotting the iso rpm lines
        for rpm in rpm_set:
            tmp = np.array([point for point in sorted_BCs if point[0] == rpm])
            tmp = np.array(list(map(lambda point: _bc_to_output(data, point), tmp)))
            plt.plot(*tmp.T, "k--")
        
        def _point_estimate_from_bcs(rpm, static_pressure, data):

            rpm_bc_data = data[data[:, 0] == rpm, :]
            rpm_bc_data = rpm_bc_data[~np.isnan(rpm_bc_data[:, 3]), :]

            pressure_bc_data = data[data[:, 1] == static_pressure, :]
            pressure_bc_data = pressure_bc_data[~np.isnan(pressure_bc_data[:, 3]), :]

            if pressure_bc_data.shape[0] < 2 or rpm_bc_data.shape[0] < 2:
                print(f'The following point could not be plotted:\n\t{rpm} rpm\t-\t{static_pressure} kPa')
                return None, None

            coef1 = np.polyfit(*rpm_bc_data[:, [4, 3]].T, 1)
            coef2 = np.polyfit(*pressure_bc_data[:, [4, 3]].T, 1)

            slope1, intercept1 = coef1
            slope2, intercept2 = coef2
            x = (intercept2 - intercept1) / (slope1 - slope2)
            y = slope1 * x + intercept1

            return x, y

        coords = np.array(
            [
                list(_point_estimate_from_bcs(point[0], point[1], data)) 
                for point in data[np.isnan(data[:, 3]), :]
            ]
        )

        if coords.shape != (0,):
            plt.scatter(*coords.T, color='red', marker='.')

        # # Add axis labels
        plt.xlabel("Mass flowrate")
        plt.ylabel("Pressure ratio")
        plt.title("Compressor map")
        plt.xlim((3, 18))
        plt.ylim((0.8, 2))

        plt.show()

def read_computations_from_csv(csv_file):
    with open(csv_file, "r") as f:
        data = f.read()

    computations = [
        tuple(computation.replace(" ", "").split(","))
        for computation in data.split("\n")
        if computation
    ]

    for i, computation in enumerate(computations):
        new = []
        for _, element in enumerate(computation):
            try:
                new.append(int(element))
            except:
                new.append(element)
        computations[i] = tuple(new)

    return computations


class FineComputation:
    def __init__(self, run_file, initial_solution_file):

        self.run_file = run_file
        self.name = f'{run_file.split("/")[-1].split(".")[-2]}'
        self.parent_folder = f'{"/".join(run_file.split("/")[:-1])}'

        self.bc = _get_bc_from_name(self.name)
        self.initial_solution_file = initial_solution_file

        self.results = None
        try:
            self.get_results()
        except IndexError:
            pass
        except Exception as e:
            raise e

    @classmethod
    def from_run_file(cls, run_file):

        # check that the path inputted exists and that the file is indeed an iec file.
        if not run_file.endswith(".run"):
            raise TypeError(
                f"{run_file} does not have the required extension (`.run`). If the extension was not included, please include it."
            )
        if not os.path.isfile(run_file):
            warn(
                f"{run_file} is either not a valid path, is not a file or the file doesn't exist. The initial solution file must be set manually."
            )
            initial_solution_file = None

        else:
            # getting the initial solution file
            with open(run_file, "r") as f:
                data = f.readlines()
            for line in data:
                if (
                    "INITIAL_SOLUTION_FILE" in line
                    and "THROUGHFLOW" not in line
                    and "RELATIVE" not in line
                ) and not line.split()[-1] == "unknown":
                    initial_solution_file = line.split()[-1]

        return cls(run_file, initial_solution_file)

    @classmethod
    def from_BCs(cls, boundary_conditions, project, initial_solution):

        run_file = _get_path_from_bc(boundary_conditions, project)

        if isinstance(initial_solution, tuple):
            initial_solution = _get_path_from_bc(initial_solution, project)

        return cls(run_file, initial_solution)

    @property
    def _is_created(self):
        return os.path.isfile(self.run_file)

    # TODO make this more robust by checking it is using the .p4pg file.
    @property
    def _is_parallelised(self):
        if self._is_created:
            with open(self.run_file, "r") as f:
                data = f.readlines()
            for line in data:
                if "NTASK" in line and len(line.split()) == 2:
                    return int(line.split()[-1]) == 23
        else:
            return False

    @property
    def _initial_solution_ready(self):
        return os.path.isfile(f"{self.initial_solution_file[:-4]}.res")

    @property
    def _has_run(self):
        return os.path.isfile(f"{self.run_file[:-4]}.res")

    @property
    def _ready_to_run(self):
        return self._is_created & self._is_parallelised & self._initial_solution_ready

    @property
    def _is_converged(self):
        if self._has_run and self.results is not None:
            return self.results.converged
        else:
            return False

    def __repr__(self) -> str:
        to_print =  (
            f"\nname:\t\t\t{self.name}\n"
            f"parent folder:\t\t{self.parent_folder}\n"
            f"full path:\t\t{self.parent_folder}/{self.name}.run\n"
            f"BCs:\t\t\t{self.bc[0]} RPM\t-\t{self.bc[1]} Pa\n"
            f"initial_solution:\t{self.initial_solution_file}\n"
            f"is ready to run?:\t{self._ready_to_run}\n"
            f"\tis created?:\t\t  {self._is_created}\n"
            f"\tis parallelised?:\t  {self._is_parallelised}\n"
            f"\tinitial solution exists?: {self._initial_solution_ready}\n"
            f"has run?:\t\t{self._has_run}\n"
            f"\tis converged?:\t{self._is_converged}\n"
        )
        if self._is_converged:
            to_print += (
                f"\tresults:\tn_iter-{self.results.iterations}\tpressure_ratio-{self.results.pressure_ratio}"
                f"\tmass_flowrate-{self.results.mass_flow_in}\tefficiency-{self.results.efficiency}\n"
            )
        return to_print + '\n'


    def get_results(self):
        if not self._has_run:
            return None

        with open(self.get_file("res"), "r") as f:
            data = f.readlines()
        # removing header
        data = data[10:]
        # retaining only the lines with global metrics
        global_data = [line.split() for line in data if line.split()[0].isdigit()]
        # turning the data into array and retaining useful metrics only
        result_array = np.array(global_data, dtype=float)[:, [0, 3, 5, 6, 7, 8, 9]]
        self.results = Results(result_array)

    def get_file(self, extension):
        return f"{self.run_file[:-4]}.{extension}"


@dataclass
class Results:

    result_array: np.ndarray
    mass_flow_in: float
    mass_flow_out: float
    pressure_ratio: float
    efficiency: float
    axial_thrust: float
    torque: float
    iterations: int

    mass_flow_diff: float
    converged: bool = False

    def __init__(self, result_array: np.ndarray):
        self.result_array = result_array

        self.iterations = result_array[-1, 0]
        self.axial_thrust = result_array[-1, 1]
        self.torque = result_array[-1, 2]
        self.efficiency = result_array[-1, 3]
        self.pressure_ratio = result_array[-1, 4]
        self.mass_flow_in = result_array[-1, 5]
        self.mass_flow_out = result_array[-1, 6]

        self.mass_flow_diff = (
            self.mass_flow_out - self.mass_flow_in
        ) / self.mass_flow_in
        self.converged = self.mass_flow_diff < 0.001 and self.iterations > 1000


def _get_bc_from_name(name):
    return int(name.split("_")[-2][:-3]), int(name.split("_")[-1][:-2]) * 10**3


def _get_path_from_bc(bc, project):
    computation_name = f"{project.name}_{bc[0]}rpm_{int(bc[1]/1000)}kp"
    return f"{project.parent_folder}/{computation_name}/{computation_name}.run"


def _get_name_from_bc(bc, project):
    return f"{project.name}_{bc[0]}rpm_{int(bc[1]/1000)}kp"
