import os

import parageom.ressources.path as pg_path

INIT_FILES_PATH = os.path.dirname(pg_path.__file__)
AG5_SCRIPT = "autogrid_script_template.py"
FT_COMPUTATIONS_SCRIPT_DGEN = "new_computation_DGEN_mesh.py"


def make_ag_script(options, script_output_file="ag_script.py"):

    with open(f"{INIT_FILES_PATH}/{AG5_SCRIPT}", "r") as f:
        data = f.read()

    for key, value in options.items():
        data = data.replace(key, value)

    with open(script_output_file, "w") as f:
        f.write(data)


def make_new_computations_script(
    options, script_output_file="fine_script.py", mesh="DGEN"
):

    if mesh == "DGEN":
        with open(f"{INIT_FILES_PATH}/{FT_COMPUTATIONS_SCRIPT_DGEN}", "r") as f:
            data = f.read()

    for key, value in options.items():
        data = data.replace(key, value)

    with open(script_output_file, "w") as f:
        f.write(data)
