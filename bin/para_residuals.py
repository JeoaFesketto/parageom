#! /usr/bin/env python
from math import *
import matplotlib.pyplot as plt
import argparse
import numpy as np
from parageom.bin_functions.preprocessing import read_residuals
from parageom.common import print_parageom

print_parageom()

#managment of user input

parser = argparse.ArgumentParser(
    description="Read residuals. Plot metrics sequentially and optionally write to csv file with -csv flag."
)

parser.add_argument(
    "res_file",
    help="Relative path to the res file where the residuals should be read.",
    type=str,
)
parser.add_argument(
    "-c",
    "--csv",
    help="Use this flag to write a csv file.",
    action="count",
    default=0,
)
parser.add_argument(
    "-o",
    "--output_folder",
    help="Output folder for the csv file if the -csv flag is used. Default is where this command is run.",
    type=str,
    default=".",
)
parser.add_argument(
    "-p",
    "--no_plot",
    help="Use this flag to refrain from plotting the metrics.",
    action="count",
    default=0,
)
args = parser.parse_args()


# defining input and output filenames from arguments
FILENAME = args.res_file
OUTPUT_FILENAME = f'{args.output_folder}/{args.res_file[:-4]}.csv'

usefull_info_array = read_residuals(FILENAME)

if args.csv:
    np.savetxt(
        OUTPUT_FILENAME, 
        usefull_info_array,
        delimiter=', ',
        header="iterations\taxial thrust\ttorque\tefficiency\tpressure ratio\tmflow in\tmflow out"
        )

if not args.no_plot:
    metrics = "axial_thrust\ttorque\tefficiency\tpressure_ratio\tmflow_in\tmflow_out".split('\t')
    for i, metric in enumerate(metrics):
        fig, ax = plt.subplots()
        ax.plot(*usefull_info_array[:, [0, i+1]].T)
        ax.set_title(metric)
        plt.show()

