#!/usr/bin/env python3
import argparse

from parageom.common import print_parageom_parablade
from parageom.geometry import Geometry


print_parageom_parablade()

# ARGUMENT DEFINITION

parser = argparse.ArgumentParser(
    description="Prepare files to be meshed and optionally mesh them in the process."
)
parser.add_argument(
    "trb_file",
    help="Relative path to trb file to be used as a template to mesh the configuration file.",
    type=str,
)
parser.add_argument(
    "blade_geometry_file",
    help="Geometry file to mesh. Accepted file types: .geomTurbo (sectioned), .cfg, .dat (sectioned)",
    type=str,
)
parser.add_argument(
    "-o",
    "--output_folder",
    help="Output folder to which to write all the created files and the output mesh.",
    type=str,
    default="to_run",
)
parser.add_argument(
    "-r",
    "--row_number",
    help="Row number of the blade to be modified. Starts at 1 (that's how autogrid works, not my choice).",
    type=int,
    default="1",
)
parser.add_argument(
    "-m",
    "--mesh",
    help="use this flag to mesh the geometries straight away once the scripts are created.",
    action="count",
    default=0,
)


args = parser.parse_args()

g = Geometry.from_file(args.blade_geometry_file)
g.make_mesh(
    args.trb_file, 
    output_dir=args.output_dir,
    row_number=args.row_number,
    auto_run=bool(args.mesh),
)
