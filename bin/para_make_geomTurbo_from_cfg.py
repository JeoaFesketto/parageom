#!/usr/bin/env python3
import argparse

from parageom.geometry import Geometry
from parageom.common import print_parageom_parablade

print_parageom_parablade()

# ARGUMENT DEFINITION

parser = argparse.ArgumentParser(description="Build .geomTurbo file from .cfg file")
parser.add_argument(
    "input_file",
    help="Input file to be used to generate the .geomTurbo file.",
    type=str,
)
parser.add_argument(
    "-o",
    "--output_path",
    help="relative path to output file",
    default=".",
    type=str,
)
parser.add_argument(
    "-N",
    "--Nsections",
    help="number of sections in output geomTurbo file",
    default=181,
    type=int,
)
parser.add_argument(
    "-n",
    "--Npoints",
    help="number of points in each section. If -TE and -LE are used, more points will be added",
    default=362,
    type=int,
)
parser.add_argument(
    "-LE",
    "--LE_fillet",
    help="Flag to fillet or not the leading edge",
    action="count",
    default=0,
)
parser.add_argument(
    "-TE",
    "--TE_fillet",
    help="Flag to fillet or not the trailing edge",
    action="count",
    default=0,
)
parser.add_argument(
    "-x",
    "--coordinates_order",
    help="Order in which the coordinates are set in the geomTurbo file. 'xyz' is default.",
    default="xyz",
    type=str,
)

args = parser.parse_args()


g = Geometry.from_file(args.input_file)

g.output_geomTurbo(
    output_folder=args.output_path,
    N_sections=args.Nsections,
    N_points=args.Npoints,
    LE_fillet=bool(args.LE_fillet),
    TE_fillet=bool(args.TE_fillet),
    xyz=args.coordinates_order,
)

if args.input_file.endswith('.geomTurbo'):
    print('Trying to be smart? I did it anyways :p .')