#!/usr/bin/env python3
import argparse

from parageom.common import print_parageom
from parageom.bin_functions.fine_project import FineProject

print_parageom()

# ARGUMENT DEFINITION

parser = argparse.ArgumentParser(
    description="Show all the computations inside a project."
)
parser.add_argument(
    "iec_file",
    help="Relative path to iec file.",
    type=str,
)
parser.add_argument(
    "-r",
    "--rpm",
    help="Filter by RPM.",
    type=int,
    default=0,
)
parser.add_argument(
    "-p",
    "--pressure",
    help="Filter by outlet static pressure.",
    type=int, 
    default=0,
)
parser.add_argument(
    "-c",
    "--converged_only",
    help="Flag to show only converged computations.",
    action='count',
    default=0
)
parser.add_argument(
    "-nc",
    "--not_converged_only",
    help="Flag to show only non converged computations.",
    action='count',
    default=0
)

args = parser.parse_args()

rpm = None if args.rpm == 0 else args.rpm
pressure = None if args.pressure == 0 else args.pressure
converged_filter = None
if args.converged_only:
    converged_filter = True
if args.not_converged_only:
    if converged_filter:
        raise Exception('Both flags -c and -nc are contradictory and can\'t be used together')
    else:
        converged_filter = False

fp = FineProject(args.iec_file)

fp.show_computations(rpm = rpm, pressure = pressure, converged_only = converged_filter)