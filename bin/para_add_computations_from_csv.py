#!/usr/bin/env python3
import argparse

from parageom.common import print_parageom
from parageom.bin_functions.fine_project import FineProject, read_computations_from_csv

print_parageom()

# ARGUMENT DEFINITION

parser = argparse.ArgumentParser(
    description="Add computations to an iec file."
)
parser.add_argument(
    "iec_file",
    help="Relative path to iec file in which to create the additional computations.",
    type=str,
)
parser.add_argument(
    "csv_file",
    help="Relative path to the csv file containing the computations to be created. Each line should contain an rpm value and a pressure value separated by a coma.",
    type=str,
)


args = parser.parse_args()

fp = FineProject(args.iec_file)
computations = read_computations_from_csv(args.csv_file)

fp.add_multiple_computations(computations)

new_computations = fp.make_multiple_computations()

fp.show_computations(computations_list = new_computations)