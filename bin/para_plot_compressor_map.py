#! /usr/bin/env python
from math import *
import argparse

from parageom.bin_functions.fine_project import FineProject
from parageom.common import print_parageom

print_parageom()

#managment of user input

parser = argparse.ArgumentParser(
    description="Read residuals and output usefull results to a csv file."
)

parser.add_argument(
    "iec_file",
    help="Relative path to the iec project file.",
    type=str,
)
parser.add_argument(
    "-s",
    "--save_results",
    help="Path to the output file.",
    type=str,
    default=".",
)
args = parser.parse_args()

fp = FineProject(args.iec_file)

if args.save_results != '.':
    fp.save_results_to_csv(args.save_results)

fp.plot_compressor_map()