#!/usr/bin/env python3
import os
import argparse

from parageom.common import print_parageom_parablade
from parageom.bin_functions.preprocessing import show_section

print_parageom_parablade()

# ARGUMENT DEFINITION

parser = argparse.ArgumentParser(
    description="Plot a specified section of one or more geomTurbo files"
)
parser.add_argument(
    "geomTurbo_files", 
    help="geomTurbo files to plot.", 
    type=str, 
    nargs="+"
)
parser.add_argument(
    "-s",
    "--span_percentage",
    help="position along the span to plot",
    default=0,
    type=int,
)

args = parser.parse_args()

show_section(
    *args.geomTurbo_files,
    span_percentage=args.span_percentage,
)

for file in args.geomTurbo_files:
    if file.endswith('.cfg'):
        os.system(f"rm -rf tmp_geomturbo")
        continue